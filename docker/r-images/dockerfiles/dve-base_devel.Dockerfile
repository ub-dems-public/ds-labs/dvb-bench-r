FROM rocker/tidyverse:devel

LABEL org.opencontainers.image.licenses="GPL-2.0-or-later" \
      org.opencontainers.image.source="https://github.com/rocker-org/rocker-versioned2" \
      org.opencontainers.image.vendor="ub-dems" \
      org.opencontainers.image.authors="DsUser DEMS <dsuser.dems@gmail.com>"

ENV TERM=xterm

COPY scripts /rocker_scripts

RUN /rocker_scripts/init_dve-userconf.sh

EXPOSE 8787

CMD ["/init"]
#CMD ["R"]
